<?php


namespace App\Organization;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class Organization extends DB
{

    private $id;
    private $name;
    private $org_name;
    private $summary;


    public  function setData($allPostData=null){

        if(array_key_exists("id",$allPostData)){
            $this->id=$allPostData['id'];
        }

        if(array_key_exists("name",$allPostData)){
            $this->name=$allPostData['name'];
        }

        if(array_key_exists("org_name",$allPostData)){
            $this->org_name=$allPostData['org_name'];
        }
        if(array_key_exists("summary",$allPostData)){
            $this->summary=$allPostData['summary'];
        }
    }

    public  function store(){
        $arrData=array($this->name,$this->org_name,$this->summary);

        $query= "INSERT INTO organization (name,org_name,summary) VALUES (?,?,?)";
        $STH= $this->DBH->prepare($query); //STH=Statement Handle
        $result= $STH->execute($arrData);

        if($result){
            Message::setMessage("Data has been Stored Successfully!");
        }

        else {
            Message::setMessage("Failed!Data has not been Stored.");
        }

        Utility::redirect('create.php');
    }
}