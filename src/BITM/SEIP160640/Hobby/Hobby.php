<?php


namespace App\Hobby;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class Hobby extends DB
{

    private $id;
    private $name;
    private $hobby;


    public  function setData($allPostData=null){

        if(array_key_exists("id",$allPostData)){
            $this->id=$allPostData['id'];
        }

        if(array_key_exists("name",$allPostData)){
            $this->name=$allPostData['name'];
        }

        if(array_key_exists("hobby",$allPostData)){

            $checkedValue = implode(',', $allPostData['hobby']);
            $this->hobby=$checkedValue;
        }
    }

    public  function store(){
        $arrData=array($this->name,$this->hobby);

        $query= "INSERT INTO hobby (name,hobby) VALUES (?,?)";
        $STH= $this->DBH->prepare($query); //STH=Statement Handle
        $result= $STH->execute($arrData);

        if($result){
            Message::setMessage("Data has been Stored Successfully!");
        }

        else {
            Message::setMessage("Failed!Data has not been Stored.");
        }

        Utility::redirect('create.php');
    }
}