-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 28, 2017 at 06:11 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atomic_project_b45`
--

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE `book_title` (
  `id` int(11) NOT NULL,
  `book_name` varchar(111) NOT NULL,
  `author_name` varchar(111) NOT NULL,
  `soft_delete` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_name`, `author_name`, `soft_delete`) VALUES
(1, 'Gitanjali', 'Rabindranath', 0),
(2, 'dd', 'ggh', 0),
(3, 'ggggggggg', 'gggggggggg', 0),
(4, 'hh', 'ty', 0),
(5, 'fffffffffff', 'hhhhhhhhhhhhh', 0),
(6, '', '', 0),
(7, '', '', 0),
(8, '', '', 0),
(9, '', '', 0),
(10, '', '', 0),
(11, '', '', 0),
(12, '', '', 0),
(13, '', '', 0),
(14, '', '', 0),
(15, '', '', 0),
(16, '', '', 0),
(17, '', '', 0),
(18, '', '', 0),
(19, 'ghjhj', 'jkjlll', 0),
(20, 'fhff', 'kjjj', 0),
(21, 'khkhk', 'uoupup', 0),
(22, 'bjbk', 'jojo', 0),
(23, 'jljljl', 'oppp', 0),
(24, 'ljjl', 'opp', 0),
(25, 'khkhkk', 'jooo', 0),
(26, 'ghjhj', 'hju', 0),
(27, 'hh', 'y', 0),
(28, 'hh', 'ju', 0),
(29, 'df', 'ty', 0),
(30, 'rrr', 'yu', 0),
(31, '', '', 0),
(32, 'fffffffffff', 'we', 0),
(33, 'fffffffffff', 'qw', 0),
(34, 'ggggggggg', 'qw', 0),
(35, 'ghjhj', 'qa', 0),
(36, 'df', '123', 0),
(37, 'fgg', 'rt', 0),
(38, 'nh', 'mn', 0),
(39, 'bjbk', 'v', 0),
(40, 't', 'ty', 0),
(41, 't', 't', 0),
(42, 'm', 'm', 0),
(43, 'fgg', 'bn', 0),
(44, 'hh', 'hh', 0),
(45, 'bb', 'mn', 0),
(46, 'mk', 'mk', 0),
(47, 'cv', 'gh', 0),
(48, 'fg', 'gh', 0),
(49, 'ggggggggg', 'y', 0),
(50, 'h', 'k', 0),
(51, 'n', 'k', 0),
(52, 'bb', 'jkjlll', 0);

-- --------------------------------------------------------

--
-- Table structure for table `dob`
--

CREATE TABLE `dob` (
  `id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `dob` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dob`
--

INSERT INTO `dob` (`id`, `name`, `dob`) VALUES
(1, 'ty', '2017-01-03');

-- --------------------------------------------------------

--
-- Table structure for table `profile_pic`
--

CREATE TABLE `profile_pic` (
  `id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `image` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile_pic`
--

INSERT INTO `profile_pic` (`id`, `name`, `image`) VALUES
(1, 'nm', 0x4a656c6c79666973682e6a7067),
(2, 'nm', 0x50656e6775696e732e6a7067),
(3, 'jk', 0x4c69676874686f7573652e6a7067),
(4, 'nm', 0x4b6f616c612e6a7067),
(5, '', 0x4465736572742e6a7067);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dob`
--
ALTER TABLE `dob`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_pic`
--
ALTER TABLE `profile_pic`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `dob`
--
ALTER TABLE `dob`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `profile_pic`
--
ALTER TABLE `profile_pic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
