<?php

require_once("../../../vendor/autoload.php");

use App\Message\Message;

if( (!isset($_SESSION)))
    session_start();
    $msg= Message::getMessage();
if($msg)
    {
    echo "<div class='footer'>$msg </div>";
    $_SESSION['message'] = "";
    }

    ?>


    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Book Title Create Form</title>
        <link rel="stylesheet" href="../../../resource/css/formstyle.css">

    </head>
    <body>
    <div class="container">
    <form action="store.php" method="post">
        <h1>Book's Information </h1>
        <input type="text" name="bookName" placeholder="Enter Book Title"><br>
        <input type="text" name="authorName" placeholder="Enter Author Name"><br>
        <input type="submit" value="Add"  name="btnAdd" class="button">
        <input type="submit" value="Add & Save" class="button">
        <input type="submit" value="Reset" class="button">
    </form>
        </div>

    <script type="text/javascript" src="../../../resource/js/jquery-3.1.1.min.js"></script>
    <script>

        jQuery (function($){

            $('.footer').fadeOut(550);
            $('.footer').fadeIn(550);
            $('.footer').fadeOut(550);
            $('.footer').fadeIn(550);
            $('.footer').fadeOut(550);
        })
    </script>
    </body>
    </html>

