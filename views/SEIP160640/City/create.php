<?php

require_once("../../../vendor/autoload.php");

use App\Message\Message;

if( (!isset($_SESSION)))
    session_start();
    $msg= Message::getMessage();
if($msg)
    {
    echo "<div class='footer'>$msg </div>";
    $_SESSION['message'] = "";
    }

    ?>


    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>City</title>
        <link rel="stylesheet" href="../../../resource/css/formstyle.css">
        <style>
            select {
                width:105%;
                padding:12px;
                margin-top:8px;
                font-family:Arial;
                line-height:1;
                border-radius:5px;
                background: rgba(0,0,0,0.3);
                color:#ffffff;
                font-size:15px;
                -webkit-appearance:none;
                box-shadow:inset 0 0 10px 0 rgba(0,0,0,0.6);

            }

            .select-style
            {
                width: 100%;
                height: 80px;
            }

        </style>
    </head>
    <body>

    <div class="container">
    <form action="store.php" method="post">
        <h1>Add City </h1>
        <input type="text" name="name" placeholder="Enter Your Name"><br>
        <div class="select-style">
            <select name="city_name">
                <option selected>Select Any One</option>
                <option value="Dhaka">Dhaka</option>
                <option value="Chittagong" >Chittagong</option>
                <option value="Khulna" >Khulna</option>
                <option value="Barishal">Barishal</option>
                <option value="Sylhet" >Sylhet</option>
            </select>
        </div>
        <input type="submit" value="Add"  name="btnAdd" class="button">
        <input type="submit" value="Add & Save" class="button">
        <input type="submit" value="Reset" class="button">
    </form>
        </div>


    <script type="text/javascript" src="../../../resource/js/jquery-3.1.1.min.js"></script>
    <script>

        jQuery (function($){

            $('.footer').fadeOut(550);
            $('.footer').fadeIn(550);
            $('.footer').fadeOut(550);
            $('.footer').fadeIn(550);
            $('.footer').fadeOut(550);
        })
    </script>
    </body>
    </html>

