<?php


namespace App\ProfilePicture;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class ProfilePicture extends DB
{
    private $id;
    private $name;
    private $profilepic;
    private $tmpimage;
    private $uploadedimage;


    public function setData($allPostData = null)
    {

        if (array_key_exists("id", $allPostData)) {
            $this->id = $allPostData['id'];
        }

        if (array_key_exists("name", $allPostData)) {
            $this->name = $allPostData['name'];
        }

    }


    public function setimage($files=null)
    {

        if (array_key_exists("profilePic", $files)) {
            $this->profilepic = $files['profilePic']['name'];
            $this->tmpimage = $files['profilePic']['tmp_name'];

        }
    }

    public function uploadimage(){


       $this->upload($this->tmpimage, $this->profilepic);
    }

    public function upload($image1,$image2)
    {

        $filename = $image2;
        $path = $image1;
        $newpath = "../../../views/SEIP160640/ProfilePicture/uploads/" . $filename;
        $img=move_uploaded_file($path, $newpath);
        if($img){echo "Image moved successfully";}

    }

   public  function store(){
        $arrData=array($this->name,$this->profilepic);

        $query= "INSERT INTO profile_pic(name,image) VALUES (?,?)";
        $STH= $this->DBH->prepare($query); //STH=Statement Handle
        $result= $STH->execute($arrData);

        if($result){
            Message::setMessage("Image Uploaded Successfully!");
        }

        else {
            Message::setMessage("Failed!Image has not been Uploaded.");
        }

        Utility::redirect('create.php');
    }

}